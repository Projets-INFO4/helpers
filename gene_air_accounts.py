#! /usr/local/bin/python
# -*- coding: utf-8 -*-
import sys
import random
import csv


def gene_passwd(pw_length=8):
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    pw = ""
    
    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        pw = pw + alphabet[next_index]

    return pw
        
def generate_accounts_file(mails_filename, csv_filename):
    print(mails_filename)
    print(csv_filename)
    with open(mails_filename, 'r') as mails_file:
        with open(csv_filename, 'w', newline='', encoding='utf-8') as csv_file:
            #writer = csv.writer(csv_file, delimiter='')
            for line in mails_file:
                mail = line.rstrip()
                t = mail.split('@')
                print(t)
                username = t[0]
                t = username.split('.')
                print(t)
                firstname = t[0]
                if (len(t) > 1):            
                    lastname = t[1]
                else:
                    lastname = ''
                passwd = gene_passwd()

                #user1,pass1,user1@example.org,John Doe,
                linecsv =  username + ',' + passwd + ',' + mail + ',' + firstname + ' ' + lastname
                #print(linecsv.encode('utf-8'))
                
                csv_file.write(linecsv+u'\n')
                #writer.writerow(linecsv)


if len(sys.argv) < 3:
    print("two filenames needed")
else:
    generate_accounts_file(sys.argv[1],sys.argv[2])

