#!/usr/bin/env python3

# python sendmail.py ricm4-14-15-air-accounts.csv mot_de_passe_sur smtps.ujf-grenoble.fr

import time

SMTPserver = 'smtps.univ-grenoble-alpes.fr'
sender =     'olivier.richard@imag.fr'

#cc = ['olivier.richard@imag.fr', 'didier.donsez@imag.fr', 'georges-pierre.bonneau@univ-grenoble-alpes.fr']
#cc = ['olivier.richard@imag.fr', 'nicolas.palix@imag.fr']
cc = ['olivier.richard@imag.fr', 'didier.donsez@imag.fr']
#cc = ['olivier.richard@imag.fr', 'nicolas.palix@imag.fr']
#cc = ['olivier.richard@imag.fr']

USERNAME = "richarol"

# typical values for text_subtype are plain, html, xml
text_subtype = 'plain'

content_pre="""\
         Bonjour,
   Veuillez trouver ci-dessous les informations relatives à votre compte wiki sur http://air.imag.fr

"""
content_post="""\


     Bien cordialement,
     Olivier Richard
"""

subject="Votre compte wiki sur air.imag.fr "

import sys
import os
import re

from smtplib import SMTP as SMTP
from email.mime.text import MIMEText

def sendmail(destination, content, passwd):
    print("send mail to " + destination)
    try:
        msg = MIMEText(content, text_subtype)
        msg['To'] = destination
        msg['Subject']= subject
        msg['From']  = sender # some SMTP servers will do this automatically, not all
        msg['Cc'] = ', '.join(cc)

        smtpserver = SMTP(host=SMTPserver, port=587)
        smtpserver.ehlo()
        smtpserver.starttls() #Use encrypted connection
        smtpserver.ehlo()
        smtpserver.login(USERNAME, passwd)

        smtpserver.sendmail(sender, [destination] + cc, msg.as_string())
        
    except Exception as exc:
        sys.exit( "mail failed; %s" % str(exc) ) # give a error message

def extract_and_send(filename, passwd_mail=None):
    print(filename)
    with open(filename, 'r') as account_file:
        for line in account_file:
            l = line.rstrip()
            print(l)
            t = l.split(',')
            username = t[0]
            passwd = t[1]
            mail = t[2]

            user_passwd = "User/Password: " + username + " / " + passwd
            content = content_pre +  user_passwd + content_post
        
            print(content)

            if passwd_mail:
                #sendmail("olivier.auguste.richard@gmail.com", content, passwd_mail)
                sendmail(mail, content, passwd_mail)
                print('Be patient, sleeping a bit to not trigger suspicion')
                time.sleep(5)

if len(sys.argv) == 1:
    print("filename needed")
else:
    if len(sys.argv) == 2:
        extract_and_send(sys.argv[1])
    else:
        extract_and_send(sys.argv[1],sys.argv[2])
