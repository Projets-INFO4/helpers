#!/usr/bin/env python3

# nix-shell -p 'python37.withPackages(ps: with ps; [ python-gitlab ])'
# https://docs.gitlab.com/ee/api/README.html

# https://python-gitlab.readthedocs.io/en/stable/
# 50 => Owner access # Only valid for groups

import gitlab
import csv


grp_18_19 = 2999
grp_19_20 = 5559

def get_token(secret_file='scripts/secret.txt'):
    token = ""
    with open(secret_file) as f:
        token  = f.readline()
    return token[:-1]

gl = gitlab.Gitlab('https://gricad-gitlab.univ-grenoble-alpes.fr/', get_token())
#Projets-INFO4 19-20 Groupe: 5559


def get_grp(grp_id):
    return gl.groups.get(grp_id)

def get_user_id(username):
    users = gl.users.list(search=username)
    if len(users) != 1:
        print('Bad: len(users): {}'.format(len(users)))
        raise ValueError
    return users[0].id

def delete_member(grp, username):
    user_id = get_user_id(username)
    grp.members.delete(user_id)
    
def add_user(grp_name, subgrp_name):
    pass

def get_sub_grp(grp_name, subgrp_name):
    grp = get_grp(grp_name)
    sub_grps = grp.subgroups.list(search=subgrp_name)
    if not sub_grps:
        return []
    elif len(sub_grps) > 1:
        print('Bad: len(sub_grps): {}'.format(len(sub_grps)))
        raise ValueError
    else:
        return sub_grps[0]

def create_grp_prj(grp_name, subgrp_name, usernames):
    #create subproject
    print('Create subproject: {}/{}'.format(grp_name, subgrp_name))
    new_grp = gl.groups.create({'name': subgrp_name, 'path': subgrp_name,
                                'parent_id': str(grp_name),
                                'visibility': 'public'})
    # add users
    print('Add users {}'.format(usernames))
    for username in usernames:
        new_grp.members.create({'user_id': get_user_id(username),
                                'access_level': gitlab.OWNER_ACCESS})
    # create docs
    print('Create docs repository')
    project = gl.projects.create({'name': 'docs', 'namespace_id': new_grp.id,
                                  'visibility': 'public'})

def create_groups_projects(grp_name, subgroups, subgrps_users):
    
    for subgrp_id in subgroups:
        if not get_sub_grp(grp_name, str(subgrp_id)):
            create_grp_prj(grp_name, str(subgrp_id), subgrps_users[subgrp_id])
        else:
            print('Skip subgroup creation, {} is already there'.format(subgrp_id))

def read_cvs(filename='etudiants_projets.csv'):
    prjs = []
    prj_user = {}
    prj_user_names = {}
    etu_prj = []
    r_prj = 3
    r_user = 4

    flag = 0
    with open(filename, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            etu_prj.append(row)
            if flag:
                if row[r_prj]:
                    prj = int(row[r_prj])
                    if prj not in prjs:
                        prjs.append(prj)
                        prj_user[prj] = []
                        prj_user_names[prj] = ""
                    if row[r_user]:
                        prj_user[prj].append(row[r_user])
                        prj_user_names[prj] += "{} {},".format(row[0], row[1])
            else:
                flag = 1
    prjs.sort()
    return (prjs, prj_user, prj_user_names)


def wiki_table(subgroups, subgrps_user_names, years="19-20"):
    table =  ('|class="wikitable alternance"\n'
               '|+ Affectation des projets INFO4 {}\n'
               '|-\n'
               '|\n'
               '!scope="col"| Sujet\n'
               '!scope="col"| Etudiants\n'
               '!scope="col"| Enseignant(s)\n'
               '!scope="col"| Fiche de suivi\n'
               '!scope="col"| Documents\n'
               '|-\n').format(years)

    for subgrp_id in subgroups:
        names = subgrps_user_names[subgrp_id]
        table += (               
            '!scope="row"| {}\n'
            '| [[TODO]]\n'
            '| {}\n'
            '| TODO\n'
            '| [https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/{}/{}/docs/README.md Fiche]\n'
            '| [[Media:xxx.pdf|Rapport final]] - [[Media:xxx.pdf|Presentation finale FR]] - [[Media:xxx.pdf|Final Presentation EN]] - [[Media:xxx.pdf|Flyer]] - [[Media:xxx.pdf|Presentation de mi-parcours]]\n'
            '|-\n').format(subgrp_id, names[:-1], years, subgrp_id)

    return '{' + table + '|}'

        
#grp = get_grp(grp_18_19)
#new_grp = gl.groups.create({'name': '42', 'path': '42', 'parent_id': str(grp_18_19)})
#sub_grp = grp.subgroups.list(search='42')[0]

# Example only one subgroup w/ docs repository
#sb_ids, sb_users, sb_user_names = read_cvs()
#create_groups_projects(grp_19_20, sb_ids[:1], sb_users)

